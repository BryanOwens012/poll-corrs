# Poll Corrs

Political poll correlations

Right now, the only correlation I've made is between Biden's disapproval ratings (from FiveThirtyEight) and S&P closing prices (from MarketWatch).

## Sources

**Author**: Bryan Owens

**Date**: 12/1/2021

**FiveThirtyEight data**: scraped from the source code of the [FiveThirtyEight Biden Approval Rating webpage](https://projects.fivethirtyeight.com/biden-approval-rating/)

**S&P 500 data**: downloaded from [MarketWatch](https://www.marketwatch.com/investing/index/spx/download-data?startDate=1/23/2021&endDate=12/1/2021)